<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/welcome', function (Request $request) {
    $user = $request->input('user');
    return $user;
});

Route::get('sm/test',function(){
    echo url('/');
});


//Route::post('/sm/saveUserProfile', 'MobileController@saveUserProfileDataPost');
//Route::post('/sm/updateUserProfile', 'MobileController@updateUserProfile');
//Route::post('/sm/updateProfilePic', 'MobileController@updateProfilePic');

//get featured
Route::get('/sm/getFeatured','MobileController@getFeatured');
//get random products
Route::get('/sm/getProductsNew','MobileController@getNew');

Route::get('/sm/getProductsAll','MobileController@getAllProducts');

//get Category
Route::get('/sm/getCategory','MobileController@getCategorys');
//get products by category
Route::post('sm/getProductsByCategory','MobileController@getProductsByCategory');
//get Brand
Route::get('/sm/getBrands','MobileController@getBrands');
Route::get('/sm/getBrandsCategory','MobileController@getBrandsCategory');

//get Adds
Route::get('sm/getAds','MobileController@getAdverts');
//Get products by brand id
Route::post('sm/getProductsByBrand','MobileController@getProductsByBrand');
//products images
Route::post('sm/getProductImages','MobileController@getProductImages');
//check out
Route::post('sm/checkout','MobileController@checkout');
//order history
Route::post('sm/orderhistory','MobileController@orderHistory');
//Add shipping address
Route::post('sm/shippingAddress','MobileController@shippingAddress');
//Update Shipping address
Route::post('sm/shippingAddressUpdate','MobileController@updateShipping');
//Save Shopper to database
Route::post('sm/saveShopper','MobileController@saveShopper');
//Update Shopper to database
Route::post('sm/updateShopper','MobileController@updateShopper');
//Update profile pic
Route::post('sm/updateProfilePic','MobileController@updateProfilePic');
//Shoper shipping address
Route::post('sm/getShippingAddress','MobileController@getShoppingAddress');
//Get Counties
Route::get('sm/getCounties','MobileController@getCounties');
//Get Tags
Route::get('sm/getTags','MobileController@getTags');

Route::get('sm/getTagsHome','MobileController@getTagsHome');
//Get Products by a Tag
Route::post('sm/getProductsTags','MobileController@getProductsByTags');
//double tap product
Route::post('sm/doubleTap','MobileController@doubleTap');
//un double tap product
Route::post('sm/undoubleTap','MobileController@unDoubleTap');
//double taps count
Route::post('sm/tapsCount','MobileController@tapsCount');
//check if double taped
Route::post('sm/doubletapcheck','MobileController@checkUserDoubleTaps');
//Get similar products
Route::post('sm/getSimilarProducts','MobileController@similarProducts');
//Payment Method
Route::get('sm/getPaymentMethod','MobileController@paymentMethod');
//order history
Route::post('sm/history-order-details','MobileController@orderHistoryProducts');
//Order Status
Route::post('sm/order-status','MobileController@orderStatus');
//Get active orders
Route::post('sm/getActiveOrders','MobileController@ordersActive');
//Get unactive orders
Route::post('sm/getinactiveOrders','MobileController@unActiveOrders');
//Get sliders
Route::get('sm/slider','MobileController@slider');
//Get filter
Route::post('sm/filter','MobileController@filter');
//Wish list send
Route::post('sm/wishlistsend','MobileController@wishlistpost');
//Wish list get
Route::post('sm/wishlistDisplay','MobileController@wishlistDisplay');
//Wishlist remove
Route::post('sm/wishlistRemove','MobileController@wishlistRemove');
//wishlist check
Route::post('sm/wishlistcheck','MobileController@checkifwishlisted');

Route::get('sm/getProductsByCategoryShoes','MobileController@getProductsByCategoryShoes');

Route::get('sm/getProductsByCategoryFragrance','MobileController@getProductsByCategoryFragrance');

Route::get('sm/getProductsByCategoryWatch','MobileController@getProductsByCategoryWatch');

Route::get('sm/getProductsByCategoryOther','MobileController@getProductsByCategoryOther');

Route::post('sm/getBrandsByCatgeory','MobileController@getBrandsByCategory');

Route::post('sm/getTagsByCatgeory','MobileController@getTagsByCategory');
