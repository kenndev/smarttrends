<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return redirect('login');
});



Auth::routes();
Route::get('register/verify/{token}', 'Auth\RegisterController@verify');



Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    //Settings
    Route::get('settings', 'AdminController@settings')->name('settings');
    Route::post('settings', 'AdminController@settingsAdd')->name('settings.add');

    //Products
    Route::get('products', 'AdminController@products')->name('products');
    Route::post('products', 'AdminController@productsAdd')->name('products.add');
    Route::get('productsTable', 'AdminController@productsTable')->name('products.table');
    Route::get('products-details/{id}', 'AdminController@productDetails')->name('products.details');
    Route::get('products-delete/{id}', 'AdminController@productDelete')->name('productsdelete');
    Route::get('products-edit/{id}', 'AdminController@editProducts')->name('product.edit');
    Route::get('products-add', 'AdminController@addProducts')->name('product.new.add');
    Route::get('products-tags2/{id}', 'AdminController@productTags')->name('product.get.tags');

    Route::get('product-feature/{id}', 'AdminController@productFeature')->name('product.feature');
    Route::get('product-unfeature/{id}', 'AdminController@productUnfeature')->name('product.unfeature');
    Route::get('product-activate/{id}', 'AdminController@productActivate')->name('product.activate');
    Route::get('product-deactivate/{id}', 'AdminController@productDeactivate')->name('product.deactivate');

    //Category
    Route::get('category', 'AdminController@category')->name('category');
    Route::post('category', 'AdminController@categoryAdd')->name('category.add');
    Route::get('categoryTable', 'AdminController@categoryTable')->name('categorytable');
    Route::get('category-details/{id}', 'AdminController@categoryDetails')->name('category.details');
    Route::get('category-delete/{id}', 'AdminController@categoryDelete')->name('category.delete');


    //Brand
    Route::get('brand', 'AdminController@brand')->name('brand');
    Route::post('brand', 'AdminController@brandAdd')->name('brandadd');
    Route::get('brandTable', 'AdminController@brandTable')->name('brandtable');
    Route::get('brand-details/{id}', 'AdminController@brandDetails')->name('brand.details');
    Route::get('brand-delete/{id}', 'AdminController@brandDelete')->name('brand.delete');
    Route::get('get-brands', 'AdminController@getBrandsByCategory')->name('brand.category');

    //Adverts
    Route::get('adverts', 'AdminController@adverts')->name('adverts');
    Route::post('adverts', 'AdminController@advertsAdd')->name('advertsadd');
    Route::get('advertsTable', 'AdminController@advertsTable')->name('adverts.table');
    Route::get('brand-details/{id}', 'AdminController@brandDetails')->name('brand.details');
    Route::get('adverts-delete/{id}', 'AdminController@advertsDelete')->name('adverts.delete');

    //Product Images
    Route::get('productImages/{id}', 'AdminController@productImages')->name('productImages');
    Route::get('productImagesTable/{id}', 'AdminController@productImagesTable')->name('productImagesTable');
    Route::post('productImagesAdd', 'AdminController@productImagesAdd')->name('productImagesAdd');
    Route::get('productImagesDelete/{id}', 'AdminController@productImagesDelete')->name('productImagesDelete');

    Route::get('tags', 'AdminController@tags')->name('tags');
    Route::post('tags-add', 'AdminController@tagsAdd')->name('tags.add');
    Route::get('tags-details/{id}', 'AdminController@tagsDetails')->name('tags.details');
    Route::get('tags-delete/{id}', 'AdminController@tagsDelete')->name('tags.delete');
    Route::get('tagsTable', 'AdminController@tagsTable')->name('tags.table');

    //colors
    Route::get('colors', 'AdminController@colors')->name('colors');
    Route::post('colors-add', 'AdminController@colorsAdd')->name('colors.add');
    Route::get('colors-details/{id}', 'AdminController@colorsDetails')->name('colors.details');
    Route::get('colors-delete/{id}', 'AdminController@colorsDelete')->name('colors.delete');
    Route::get('colorsTable', 'AdminController@colorsTable')->name('colors.table');

    //Size
    Route::get('size', 'AdminController@size')->name('size');
    Route::post('size-add', 'AdminController@sizeAdd')->name('size.add');
    Route::get('size-details/{id}', 'AdminController@sizeDetails')->name('size.details');
    Route::get('size-delete/{id}', 'AdminController@sizeDelete')->name('size.delete');
    Route::get('sizeTable', 'AdminController@sizeTable')->name('size.table');

    //Payment method
    Route::get('payment', 'AdminController@paymentMethod')->name('payment');
    Route::post('payment-add', 'AdminController@paymentMethodAdd')->name('payment.add');
    Route::get('payment-details/{id}', 'AdminController@paymentMethodDetails')->name('payment.details');
    Route::get('payment-delete/{id}', 'AdminController@paymentMethodDelete')->name('payment.delete');
    Route::get('paymentTable', 'AdminController@paymentMethodTable')->name('payment.table');

    //Order Status
    Route::get('orderStatus', 'AdminController@orderStatus')->name('order.status');
    Route::post('orderStatus-add', 'AdminController@orderStatusAdd')->name('order.status.add');
    Route::get('orderStatus-details/{id}', 'AdminController@orderStatusDetails')->name('order.status.details');
    Route::get('orderStatus-delete/{id}', 'AdminController@orderStatusDelete')->name('order.status.delete');
    Route::get('orderStatusTable', 'AdminController@orderStatusTable')->name('order.status.table');

    //Orders
    Route::get('orders', 'AdminController@orders')->name('orders.view');
    Route::get('ordersOrdered', 'AdminController@ordersOrdered')->name('orders.ordered');
    Route::get('ordersShipped', 'AdminController@ordersShipped')->name('orders.shipped');
    Route::get('ordersReceived', 'AdminController@ordersReceived')->name('orders.received');

    Route::get('ordersTableOrdered', 'AdminController@ordersOrderedTable')->name('orders.ordered.Table');
    Route::get('ordersTableShiped', 'AdminController@ordersShipedTable')->name('orders.shiped.Table');
    Route::get('ordersTableReceived', 'AdminController@ordersReceivedTable')->name('orders.received.Table');



    Route::get('ordersTable', 'AdminController@ordersTable')->name('orders.Table');
    Route::get('ordersShip', 'AdminController@ordersShip')->name('order.ship');
    Route::get('ordersdetails/{id}', 'AdminController@orderDetailsView')->name('order.details');
    Route::get('ordersdetailsTable/{id}', 'AdminController@orderDetailsTable')->name('order.details.table');
    Route::get('ordersOrderStatusChange/{id}/{status}', 'AdminController@changeOrderStatus')->name('order.status.change');
    Route::get('ordersOrderpaymentChange/{id}/{payment}', 'AdminController@changePaymentStatus')->name('order.payment.change');

    Route::get('slider', 'AdminController@slider')->name('slider');
    Route::get('slider-table', 'AdminController@sliderTable')->name('slider.table');
    Route::post('slider-add', 'AdminController@sliderAddEdit')->name('slider.add');
    Route::get('slider-details/{id}', 'AdminController@sliderDetails')->name('slider.details');
    Route::get('slider-delete/{id}', 'AdminController@sliderDelete')->name('slider.delete');

    Route::get('ml', 'AdminController@ml')->name('ml');
    Route::post('ml-add', 'AdminController@mlAdd')->name('ml.add');
    Route::get('ml-delete/{id}', 'AdminController@mlDelete')->name('ml.del');
    Route::get('ml-details/{id}', 'AdminController@mlDetails')->name('ml.details');
    Route::get('ml-table', 'AdminController@mlTable')->name('ml.table');
});
