@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Orders Received</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Orders Received</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Orders Received</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <!--<a href="javascript: ajaxmodal()" class="btn btn-outline btn-primary"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"></i>Add a payment method</a>-->

        </div>


        <table class="table table-hover table-condensed table-bordered table-striped" id="category-table">
            <thead>
                <tr>
                    <th>Customer name</th>
                     <th>Phone</th>
                    <th>No of products</th>
                    <th>Sum</th>
                    <th>Order number</th>
                    <th>Shipping Address</th>
                    <th>Order status</th>
                    <th>Payment Status</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->


    <script>
        $(document).ready(function () {
            oTable = $('#category-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{!! route('orders.received.Table') !!}',
                "columns": [
                    {data: 'name', name: 'name'},
                    {data: 'contact', name: 'contact'},
                    {data: 'no_of_products', name: 'no_of_products', 'searchable': false},
                    {data: 'totalsum', name: 'totalsum', orderable: false, searchable: false, render: function (data, type, full, meta) {
                            var total = parseInt(data) + parseInt(full['delivery_cost']);
                            return total;
                        }},
                    {data: 'orderNumber', name: 'orderNumber'},
                    {data: 'shipping_address', name: 'shipping_address'},
                    {data: 'order_status', name: 'order_status', orderable: false, searchable: false, render: function (data, type, full, meta) {
                            if (data == "ordered") {

                                return '<span class="label label-default">Ordered</span>';
                            } else if (data == "packed") {
                                return '<span class="label label-primary">Packed</span>';
                            } else if (data == "shipped") {
                                return '<span class="label label-info">Shipped</span>';
                            } else if (data == "received") {
                                return '<span class="label label-success">Received</span>';
                            } else {
                                return '<span class="label label-warning">Not available</span>';
                            }
                        }},
                    {data: 'paid', name: 'paid', orderable: false, searchable: false, render: function (data, type, full, meta) {
                            if (data == 0) {

                                return '<span class="label label-warning">Not paid</span>';
                            } else {
                                return '<span class="label label-success">Paid</span>';
                            }
                        }},
                    {data: 'action', name: 'action', 'searchable': false}
                ]
            });
        });
        function ajaxmodaledit(id) {
            var url2 = base_url + '/admin/payment-details';

            $.get(url2 + '/' + id, function (data) {
                $('#id').val(data.id);
                $('#payment_method').val(data.payment_method);
                $('#exampleNiftySideFall').modal('show');
            });
        }
    </script>
    <script>
        function checkDelete()
        {
            var chk = confirm("Are You Sure To Delete This !");
            if (chk)
            {
                return true;
            } else {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function ajaxmodal() {
            $('#id').val("");
            $('#payment_method').val("");
            $('#exampleNiftySideFall').modal('show');
        }
    </script>


    <div class="row ">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


    <!-- Modal -->
    <div class="modal fade modal-slide-in-right" id="exampleNiftySideFall" aria-hidden="true"
         aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Payment Method</h4>
                </div>
                <div class="modal-body">
                    <!-- Example Basic Form -->

                    <form autocomplete="off" role="form" method="POST" action="{{ route('payment.add') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" class="form-control" id="id" name="inputId"/>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label class="control-label" for="inputBasicFirstName">Payment Method</label>
                                <input type="text" class="form-control" id="payment_method" name="payment_method"
                                       placeholder="e.g mpesa" value="{{ old('payment_method') }}" required/>
                                @if ($errors->has('payment_method'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('payment_method') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>


                        <!-- End Example Basic Form -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->



</div>
<!-- /content area -->
@endsection


