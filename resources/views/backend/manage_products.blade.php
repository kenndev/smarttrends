@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Products</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Products</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Products</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <a href="{{route('product.new.add')}}" class="btn btn-outline btn-primary"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"></i>Add a product</a>

        </div>


        <table class=" table table-hover table-striped dataTable width-full"  id="category-table">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Category name</th>
                    <th>Product name</th>
                    <th>Current Price</th>
                    <th>Old Price</th>
                    <th>Activated</th>
                    <th>Featured</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->


    <script>
        $(document).ready(function () {
            $('#category-table').DataTable({
            processing: true,
                    serverSide: true,
                    ajax: '{!! route('products.table') !!}',
                    columns: [
                        {data: 'image', name: 'products.image', orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data) {
                                    var filename = data;
                                    return '<img src="' + filename + '" class="img-bordered img-bordered-orange" width="70" height="70" alt="">';
                                } else {
                                    return '<img src="{!! asset("packages/backend/assets/photos/placeholder.png") !!}" class="img-bordered img-bordered-orange" width="70" height="70" alt="">';
                                }
                            }},
                        {data: 'category_name', name: 'category.category_name'},
                        {data: 'productName', name: 'products.productName'},
                        {data: 'price', name: 'products.price'},
                        {data: 'old_price', name: 'products.old_price'},
                        {data: 'activated', name: 'products.activated', orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 1) {

                                    return '<span class="label label-primary">Activated</span>';
                                } else {
                                    return '<span class="label label-warning">Not Activated</span>';
                                }
                            }},
                        {data: 'featured', name: 'products.featured', orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 1) {

                                    return '<span class="label label-primary">Featured</span>';
                                } else {
                                    return '<span class="label label-warning">Not Featured</span>';
                                }
                            }},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
        });
        });
                function ajaxmodaledit(id) {

                    var url2 = '{!! url('admin/products-details') !!}';
                            $.get(url2 + '/' + id, function (data) {
                                $('#id').val(data.id);

                                $('#inputProductName').val(data.productName);
                                $('#inputOldPrice').val(data.old_price);
                                $('#inputPrice').val(data.price);
                                $('#inputSize').val(data.size);
                                $('#inputColor').val(data.color);
                                $('#selectCategory').val(data.category_id);
                                $('#selectBrand').val(data.brand_id);
                                $('#inputUnitsAvailable').val(data.units_available);
                                $('#inputProductDescription').val(data.productDescription);
                                $('#exampleNiftySideFall').modal(data);
                            });
                }
    </script>
    <script>
        function checkDelete()
        {
            var chk = confirm("Are You Sure To Delete This !");
            if (chk)
            {
                return true;
            } else {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function ajaxmodal() {
            $('#id').val("");
            $('#inputProductName').val("");
            $('#inputOldPrice').val("");
            $('#inputPrice').val("");
            $('#inputSize').val("");
            $('#inputColor').val("");
            $('#inputUnitsAvailable').val("");
            $('#inputProductDescription').val("");
            $('#exampleNiftySideFall').modal('show');
        }
    </script>


    <div class="row ">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


    <!-- Modal -->
    <div class="modal fade modal-slide-in-right" id="exampleNiftySideFall" aria-hidden="true"
         aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Products</h4>
                </div>
                <div class="modal-body">
                    <!-- Example Basic Form -->

                   



                        <!-- End Example Basic Form -->
                </div>
                <div class="modal-footer">
                   
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->



</div>
<!-- /content area -->
@endsection




