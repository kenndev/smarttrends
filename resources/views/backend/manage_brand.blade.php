@extends("layouts.backend.template")

@section("content")
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">Brands</span></h4>
        </div>

    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Brand</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
        @if (is_array(Session::get('error')))
        {{ head(Session::get('error')) }}
        @endif
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @elseif(Session::has('flash_message_error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message_error') }}
    </div>
    @endif


    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Brands</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <a href="javascript: ajaxmodal()" class="btn btn-outline btn-primary"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"></i>Add a brand</a>

        </div>


        <table class=" table table-hover table-striped dataTable width-full" id="category-table">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Brand name</th>
                    <th>Category</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /basic datatable -->


    <script>
        $(document).ready(function () {
            $('#category-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('brandtable') !!}',
                columns: [
                    {data: 'image', name: 'image', orderable: false, searchable: false, render: function (data, type, full, meta) {
                            if (data) {
                                var filename = data;
                                return '<img src="' + filename + '" class="img-bordered img-bordered-orange" width="70" height="70" alt="">';
                            } else {
                                return '<img src="{!! asset("packages/backend/assets/photos/placeholder.png") !!}" class="img-bordered img-bordered-orange" width="70" height="70" alt="">';
                            }
                        }},
                    {data: 'brand_name', name: 'brands.brand_name'},
                    {data: 'category_name', name: 'category.category_name'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
        function ajaxmodaledit(id) {
            var url2 = base_url + '/admin/brand-details';

            $.get(url2 + '/' + id, function (data) {
                $('#id').val(data.id);
                $('#inputBrandName').val(data.brand_name);
                $('#inputBrandDescription').val(data.description);
                $('#selectCategory').select2().val(data.category_id).trigger("change");
                $('#exampleNiftySideFall').modal('show');
            });
        }
    </script>
    <script>
        function checkDelete()
        {
            var chk = confirm("Are You Sure To Delete This !");
            if (chk)
            {
                return true;
            } else {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function ajaxmodal() {
            $('#id').val("");
            $('#inputBrandName').val("");
            $('#inputBrandDescription').val("");
            $('#exampleNiftySideFall').modal('show');
        }
    </script>


    <div class="row ">

    </div>

    <br>




    <!-- Footer -->
    @include("layouts.backend.footer")

    <!-- /footer -->


    <!-- Modal -->
    <div class="modal fade modal-slide-in-right" id="exampleNiftySideFall" aria-hidden="true"
         aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Brand</h4>
                </div>
                <div class="modal-body">
                    <!-- Example Basic Form -->

                    <form autocomplete="off" role="form" method="POST" action="{{ route('brandadd') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" class="form-control" id="id" name="inputId"/>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label class="control-label">Category </label>
                                <select id="selectCategory" name="selectCategory" class="select-search" required>
                                    @foreach ($category as $row)
                                    <option value="{{$row->id}}">{{$row->category_name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('selectCategory'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('selectCategory') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label class="control-label" for="inputBrandName">Brand Name</label>
                                <input type="text" class="form-control" id="inputBrandName" name="inputBrandName"
                                       placeholder="Brand Name" value="{{ old('inputBrandName') }}" required/>
                                @if ($errors->has('inputBrandName'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('inputBrandName') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label class="control-label" for="inputBrandDescription">Description</label>
                                <textarea rows="4" cols="4" type="text" class="form-control" id="inputBrandDescription" name="inputBrandDescription"
                                          placeholder="Description" value="{{ old('inputBrandDescription') }}" required></textarea>
                                @if ($errors->has('inputBrandDescription'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('inputBrandDescription') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label class="control-label" for="imageFile">Upload Image</label>
                                <input type="file" class="file-input" data-show-caption="true" data-show-upload="false" id="imageFile" name="imageFile"
                                       placeholder="Upload an Image" value="{{ old('imageFile') }}" />
                                @if ($errors->has('imageFile'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('imageFile') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!-- End Example Basic Form -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->



</div>
<!-- /content area -->
@endsection















