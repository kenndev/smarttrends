<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UrlGlobal extends Model
{
    protected $fillable = [];
    protected $table = 'url';
}
