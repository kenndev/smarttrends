<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class MobileController extends Controller {

    //
    public function getFeatured() {
        $featured = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('products.featured', 1)
                ->where('products.activated', 1)
                ->take(10)
                ->inRandomOrder()
                ->get();
        $response["result"] = "success";
        $response["featured"] = $featured;
        echo json_encode($response);
    }

    public function getNew() {
        $new = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('products.activated', 1)
                ->take(10)
                ->inRandomOrder()
                ->get();
        $response["result"] = "success";
        $response["newProduct"] = $new;
        echo json_encode($response);
    }

    public function getAllProducts() {
        $new = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('products.activated', 1)
                ->inRandomOrder()
                ->get();
        $response["result"] = "success";
        $response["newProduct"] = $new;
        echo json_encode($response);
    }

    public function getAdverts() {
        $advert = \App\Adverts::select('image', 'id as advertId', 'brand_id as brandId')->get();
        $response["result"] = "success";
        $response["advert"] = $advert;
        echo json_encode($response);
    }

    public function getCategorys() {
        $category = \App\Category::select('image', 'category_name', 'id as categoryId')->get();
        $response["result"] = "success";
        $response["category"] = $category;
        echo json_encode($response);
    }

    public function getProductsByCategory(Request $request) {
        $category_id = $request->input('category_id');
        $products = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('category_id', $category_id)
                ->where('products.activated', 1)
                ->get();
        $response["result"] = "success";
        $response["products"] = $products;
        echo json_encode($response);
    }

    public function getProductsByCategoryShoes() {
        //$category_id = $request->input('category_id');
        $products = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('products.category_id', 4)
                ->where('products.activated', 1)
                ->get();
        $response["result"] = "success";
        $response["products"] = $products;
        echo json_encode($response);
    }

    public function getProductsByCategoryFragrance() {
        //$category_id = $request->input('category_id');
        $products = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('products.category_id', 7)
                ->where('products.activated', 1)
                ->get();
        $response["result"] = "success";
        $response["products"] = $products;
        echo json_encode($response);
    }

    public function getProductsByCategoryWatch() {
        //$category_id = $request->input('category_id');
        $products = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('products.category_id', 6)
                ->where('products.activated', 1)
                ->get();
        $response["result"] = "success";
        $response["products"] = $products;
        echo json_encode($response);
    }

    public function getProductsByCategoryOther() {
        //$category_id = $request->input('category_id');

        $products = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->whereNotIn('products.category_id', [4, 6, 7])
                ->where('products.activated', 1)
                ->get();
        $response["result"] = "success";
        $response["products"] = $products;
        echo json_encode($response);
    }

    public function getBrands() {
        $brand = \App\Brand::select('image', 'brand_name', 'id as brandId', 'category_id')->orderBy('brand_name')->get();
        $response["result"] = "success";
        $response["brands"] = $brand;
        echo json_encode($response);
    }

    public function getBrandsCategory(Request $request) {
        $category_id = $request->input('category_id');
        $brand = \App\Brand::select('image', 'brand_name', 'id as brandId', 'category_id')
                        ->where('category_id', $category_id)
                        ->orderBy('brand_name')->get();
        $response["result"] = "success";
        $response["brands"] = $brand;
        echo json_encode($response);
    }

    public function getProductsByBrand(Request $request) {
        $brand_id = $request->input('brand_id');
        $products = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('brand_id', $brand_id)
                ->where('products.activated', 1)
                ->get();
        $response["result"] = "success";
        $response["products"] = $products;
        echo json_encode($response);
    }

    //Get product images
    public function getProductImages(Request $request) {
        $product_id = $request->input('product_id');
        $productimages = \App\ProductsImages::select('image_url')->where('product_id', $product_id)->get();
        if (count($productimages)) {
            $response["images"] = $productimages;
        } else {
            $productsimage2 = \App\Product::select('image as image_url')->where('id', $product_id)->get();
            $response["images"] = $productsimage2;
        }
        $response["result"] = "success";

        echo json_encode($response);
    }

    public function checkout(Request $request) {
        $product_id = $request->input('product_id');
        $shopper_id = $request->input('shopper_id');
        $size = $request->input('size');
        $color = $request->input('color');
        $quantity = $request->input('quantity');
        $order_no = $request->input('order_no');
        $payment_id = $request->input('payment_id');
        $shipping_address = $request->input('shipping_address');
        $delivery_cost = $request->input('delivery_cost');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $products = \App\Product::findorFail($product_id);
        $dateshoping = \Carbon\Carbon::now('Africa/Nairobi');
        $shoppers2 = \App\Shoppers::where('firebase_id', $shopper_id)->first();
//        
//        $checkout = new \App\Orders();
//        $checkout->product_id = $product_id;
//        $checkout->orderNumber = $order_no;
//        $checkout->price = $products->price;
//        $checkout->quantity = $quantity;
//        $checkout->total = $quantity * $products->price;
//        $checkout->size = $size;
//        $checkout->color = $color;
//        $checkout->save();
        $orderdetails = new \App\OrderDetails();
        $orderdetails->customer_id = $shoppers2->id;
        $orderdetails->orderNumber = $order_no;
        $orderdetails->payment_id = $payment_id;
        $orderdetails->order_date = $dateshoping;
        $orderdetails->shipping_address = $shipping_address;
        $orderdetails->latitude = $latitude;
        $orderdetails->longitude = $longitude;
        $orderdetails->delivery_cost = $delivery_cost;
        $orderdetails->product_id = $product_id;
        $orderdetails->orderNumber = $order_no;
        $orderdetails->price = $products->price;
        $orderdetails->quantity = $quantity;
        $orderdetails->total = $quantity * $products->price;
        $orderdetails->size = $size;
        $orderdetails->color = $color;
        $orderdetails->save();
        $response["result"] = "success";
        $response["message"] = "you have checkout successfully. Continue shopping";
        echo json_encode($response);
    }

    public function orderHistory(Request $request) {
        $shopper_id = $request->input('shopper_id');
        $shopper = \App\Shoppers::where('firebase_id', $shopper_id)->first();
        $history = \App\OrderDetails::where('customer_id', $shopper->id)
                ->groupBY('orderNumber')
                ->leftJoin('order_status', 'order_details.order_status', '=', 'order_status.id')
                ->selectRaw('(Count(product_id)) as no_of_products, sum(total) as totalsum, delivery_cost, orderNumber,order_status.order_status')
                ->get();


        if (count($history)) {
            $response["result"] = "success";
            $response["message"] = "Order history retrieved successfully";
            $response["history"] = $history;
            echo json_encode($response);
        } else {
            $response["result"] = "fail";
            $response["message"] = "no order history at the moment";
            $response["history"] = $historydetails;
            echo json_encode($response);
        }
    }

    /*
     * 
     * Shipping address
     */

    public function shippingAddress(Request $request) {
        $shopper_id = $request->input('firebase_id');
        $user = \App\Shoppers::where('firebase_id', $shopper_id)->first();
        $county_id = $request->input('county_id');
        $shipping_address = $request->input('shipping_address');
        $shipping = new \App\ShippingAdress();
        $shipping->shopper_id = $user->id;
        $shipping->county_id = $county_id;
        $shipping->address = $shipping_address;
        $shipping->save();
        $response["result"] = "success";
        $response["message"] = "Shopping Address posted successfully";
        echo json_encode($response);
    }

    public function updateShipping(Request $request) {
        $shopper_id = $request->input('firebase_id');
        $shopping_id = $request->input('shipping_id');
        $user = \App\Shoppers::where('firebase_id', $shopper_id)->first();
        $county_id = $request->input('county_id');
        $shipping_address = $request->input('shipping_address');
        $shipping = \App\ShippingAdress::findorFail($shopping_id);
        $shipping->shopper_id = $user->id;
        $shipping->county_id = $county_id;
        $shipping->address = $shipping_address;
        $shipping->save();
        $response["result"] = "success";
        $response["message"] = "Shopping Address updated successfully";
        echo json_encode($response);
    }

    public function getShoppingAddress(Request $request) {
        $shopper_id = $request->input('firebase_id');
        $shopper = \App\Shoppers::select('id as shippingId', 'county_id', 'address')->where('shopper_id', $shopper_id)->get();
        $response["result"] = "success";
        $response["message"] = "Shopping Address updated successfully";
        $response["shipping_address"] = $shopper;
        echo json_encode($response);
    }

    /**
     * Login New Shopper
     */
    public function saveShopper(Request $request) {
        $emailExists = \App\Shoppers::where('email', $request->input('email'))
                ->first();

        if ($emailExists) {
            $response["result"] = "success";
            $response["message"] = "User Data has synced successfully";
            $response["name"] = $emailExists->name;
            $response["email"] = $emailExists->email;
            $response["firebase_id"] = $emailExists->firebase_id;
            $response["gender"] = $emailExists->gender;
            $response["contact"] = $emailExists->contact;
            $response["image"] = $emailExists->image;
            $response["terms"] = $emailExists->terms;
            echo json_encode($response);
        } else {
            $myuser = new \App\Shoppers();
            $myuser->name = $request->input('name');
            $myuser->email = $request->input('email');
            $myuser->terms = $request->input('terms');
            $myuser->image = $request->input('image');
            $myuser->firebase_id = $request->input('firebase_id');
            $saved = $myuser->save();
            if ($saved) { //updating
                $response["result"] = "success";
                $response["message"] = "User Data has synced successfully";
                $response["name"] = $myuser->name;
                $response["email"] = $myuser->email;
                $response["firebase_id"] = $myuser->firebase_id;
                $response["gender"] = $myuser->gender;
                $response["contact"] = $myuser->contact;
                $response["image"] = $myuser->image;
                $response["terms"] = $myuser->terms;
                echo json_encode($response);
            } else {
                $response["result"] = "failure";
                $response["message"] = "Error saving data";
                echo json_encode($response);
            }
        }
    }

    public function updateShopper(Request $request) {
        $gender = $request->input('gender');
        $name = $request->input('name');
        $email = $request->input('email');
        $terms = $request->input('terms');
        $firebase_id = $request->input('firebase_id');
        $contact = $request->input('contact');
        $shoppers2 = \App\Shoppers::where('firebase_id', $firebase_id)->first();
        $shoppers2->gender = $gender;
        $shoppers2->email = $email;
        $shoppers2->name = $name;
        $shoppers2->terms = $terms;
        $shoppers2->contact = $contact;
        $shoppers2->save();
        if ($shoppers2->id) {
            $response["result"] = "success";
            $response["message"] = "Profile Updated Successfully";
            $response["name"] = $shoppers2->name;
            $response["email"] = $shoppers2->email;
            $response["firebase_id"] = $shoppers2->firebase_id;
            $response["gender"] = $shoppers2->gender;
            $response["contact"] = $shoppers2->contact;
            $response["image"] = $shoppers2->image;
            $response["terms"] = $shoppers2->terms;
            echo json_encode($response);
        }
    }

    public function updateProfilePic(Request $request) {
        $firebase_id = $request->input('firebase_id');
        $shoppers2 = \App\Shoppers::where('firebase_id', $firebase_id)->first();
        $destinationPath = 'packages/uploads/profile/' . \Carbon\Carbon::now()->year . '/' . \Carbon\Carbon::now()->month;
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }

        $filename = \Carbon\Carbon::now()->year . '_' . \Carbon\Carbon::now()->month . '_' . \Carbon\Carbon::now()->day . '_' . time() . '.png';
        $base64 = $request->input('file');

        // This saves the base64encoded destinationPath
        $upload_success = file_put_contents($destinationPath . '/' . $filename, base64_decode($base64));
        if ($upload_success) {
            $shoppers2->image = url('/') . '/' . $destinationPath . '/' . $filename;
            $shoppers2->save();
            $response["result"] = "success";
            $response["message"] = "Profile Picture Updated Successfully";
            $response["name"] = $shoppers2->name;
            $response["email"] = $shoppers2->email;
            $response["firebase_id"] = $shoppers2->firebase_id;
            $response["gender"] = $shoppers2->gender;
            $response["contact"] = $shoppers2->contact;
            $response["image"] = $shoppers2->image;
            $response["terms"] = $shoppers2->terms;
            echo json_encode($response);
        } else {
            $response["result"] = "failure";
            $response["message"] = "Error saving Image";
            echo json_encode($response);
        }
    }

    //Get Counties
    public function getCounties() {
        $counties = \App\COunties::select('id', 'name')->get();
        $response["result"] = "success";
        $response["message"] = "Counties retrieved successfully";
        $response["counties"] = $counties;
        echo json_encode($response);
    }

    /**
     * Tags
     */
    public function getTags() {
        $tags = \App\Tags::select('id as tagID', 'tag_name')->inRandomOrder()->get();

        $response["result"] = "success";
        $response["message"] = "Tags retrieved successfully";
        $response["tags"] = $tags;
        echo json_encode($response);
    }

    public function getTagsHome() {
        $tags = \App\Tags::select('id as tagID', 'tag_name')->take(4)->get();

        $response["result"] = "success";
        $response["message"] = "Tags retrieved successfully";
        $response["tags"] = $tags;
        echo json_encode($response);
    }

    public function getProductsByTags(Request $request) {
        $tags = $request->input('tag_id');
        $tagproducts = \App\TagsProducts::leftJoin('products', 'tags_products.product_id', '=', 'products.id')
                ->leftJoin('category', 'products.category_id', '=', 'category.id')
                ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('tags_products.tag_id', $tags)
                ->where('products.activated', 1)
                ->get();
        $response["result"] = "success";
        $response["products"] = $tagproducts;
        echo json_encode($response);
    }

    /**
     * Likes
     */
    public function doubleTap(Request $request) {
        $product_id = $request->input('product_id');
        $shopper_id = $request->input('firebase_id');
        $shoppers = \App\Shoppers::where('firebase_id', $shopper_id)->first();

        $likes = new \App\Likes();
        $likes->product_id = $product_id;
        $likes->shopper_id = $shoppers->id;
        $likes->save();



        $taps = \App\Likes::selectRaw('(Count(product_id)) as likes')
                ->where('product_id', $product_id)
                ->first();

        $products = \App\Product::findorFail($product_id);
        $products->likes = $taps->likes;

        $products->save();


        $response["result"] = "success";
        $response["likes"] = $taps;
        echo json_encode($response);
    }

    public function unDoubleTap(Request $request) {
        $product_id = $request->input('product_id');
        $shopper_id = $request->input('firebase_id');
        $shoppers = \App\Shoppers::where('firebase_id', $shopper_id)->first();
        $tapsdel = \App\Likes::where('product_id', $product_id)
                ->where('shopper_id', $shoppers->id)
                ->first();
        $tapsdel->delete();

        $taps = \App\Likes::selectRaw('(Count(product_id)) as likes')
                ->where('product_id', $product_id)
                ->first();

        $products = \App\Product::where('id', $product_id)->first();
        $products->likes = $taps->likes;
        $products->save();

        $response["result"] = "success";
        $response["likes"] = $taps;
        echo json_encode($response);
    }

    public function checkUserDoubleTaps(Request $request) {
        $product_id = $request->input('product_id');
        $shopper_id = $request->input('firebase_id');
        $shoppers = \App\Shoppers::where('firebase_id', $shopper_id)->first();
        $taps = \App\Likes::where('product_id', $product_id)
                ->where('shopper_id', $shoppers->id)
                ->first();
        if (count($taps)) {
            $response["message"] = "success";
            echo json_encode($response);
        } else {
            $response["message"] = "fail";
            echo json_encode($response);
        }
    }

    public function tapsCount(Request $request) {
        $product_id = $request->input('product_id');
        $shopper_id = $request->input('firebase_id');
        $taps = \App\Likes::selectRaw('(Count(product_id)) as likes')
                ->where('product_id', $product_id)
                ->pluck('likes');

        $shoppers = \App\Shoppers::where('firebase_id', $shopper_id)->first();
        $checkif = \App\Likes::where('product_id', $product_id)
                ->where('shopper_id', $shoppers->id)
                ->first();

        if (count($checkif)) {
            $response["message"] = "success";
        } else {
            $response["message"] = "fail";
        }

        $response["result"] = "success";
        $response["likes"] = $taps;
        echo json_encode($response);
    }

    function mergeArrayofArrays($array, $property = null) {
        return array_reduce(
                (array) $array, // make sure this is an array too, or array_reduce is mad.
                function($carry, $item) use ($property) {

            $mergeOnProperty = (!$property) ? $item : (is_array($item) ? $item[$property] : $item->$property);

            return is_array($mergeOnProperty) ? array_merge($carry, $mergeOnProperty) : $carry;
        }, array()); // start the carry with empty array
    }

    function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    public function similarProducts(Request $request) {
        $product_id = $request->input('product_id');
        $tags_of_pro = \App\TagsProducts::where('product_id', $product_id)->get();

        $object = new Collection();
        foreach ($tags_of_pro as $row) {
            $prducts_reslts = \App\TagsProducts::rightJoin('products', 'tags_products.product_id', '=', 'products.id')
                    ->rightJoin('category', 'products.category_id', '=', 'category.id')
                    ->rightJoin('brands', 'products.brand_id', '=', 'brands.id')
                    ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                    ->where('tags_products.tag_id', $row->tag_id)
                    ->inRandomOrder()
                    ->get();
//            $array_o[$key] = $prducts_reslts;
            $object = $object->merge($prducts_reslts)->unique()->take(20);
        }
//        $array_o[] = $object->toArray();
//        $array =  (array) $object;
//           $arraynew = array($object); 
//        $result = array_unique($arraynew);
//         print_r ($result);
        //$final_array = $object->unique();

        $final_array = $object;

//        $yy = $this->mergeArrayofArrays($array_o);
        //$final_array = $this->unique_multidim_array($result, 'productName');
        $response["result"] = "success";
        $response["similar"] = $final_array;


        echo json_encode($response);
    }

    /**
     * Payment Methods
     */
    public function paymentMethod() {
        $payment = \App\PaymentMethod::select('id as paymentMethodId', 'payment_method')->get();
        $response["result"] = "success";
        $response["payment_methods"] = $payment;
        echo json_encode($response);
    }

    //Get items in order
    public function orderHistoryProducts(Request $request) {
        $order_id = $request->input('orderNum');
        $historydetails = \App\OrderDetails::leftJoin('products', 'order_details.product_id', '=', 'products.id')
                ->leftJoin('category', 'products.category_id', '=', 'category.id')
                ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
                ->select('order_details.size', 'order_details.color', 'order_details.quantity', 'order_details.order_date', 'order_details.shipping_address', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('orderNumber', $order_id)
                ->get();
        $response["result"] = "success";
        $response["order_products"] = $historydetails;
        echo json_encode($response);
    }

    //Get order status
    public function orderStatus(Request $request) {
        $order_id = $request->input('orderNum');
        $orders = \App\OrderDetails::where('orderNumber', $order_id)
                ->leftJoin('order_status', 'order_details.order_status', '=', 'order_status.id')
                ->select('order_status.order_status')
                ->first();
        $response["result"] = "success";
        $response["order_status"] = $orders;
        echo json_encode($response);
    }

    public function activeOrder(Request $request) {
        $firebaseID = $request->input('firebase_id');
        var_dump($firebaseID);
    }

    public function ordersActive(Request $request) {
        $firebaseID = $request->input('firebase_id');
        $senderID = \App\Shoppers::where('firebase_id', $firebaseID)->select('id')->first();
        $history = \App\OrderDetails::where('customer_id', $senderID->id)
                ->where('order_details.order_status', '!=', 3)
                ->leftJoin('order_status', 'order_details.order_status', '=', 'order_status.id')
                ->groupBY('orderNumber')
                ->selectRaw('order_status.order_status,(Count(product_id)) as no_of_products, sum(total) as totalsum, delivery_cost, orderNumber')
                ->get();
        $response["result"] = "success";
        $response["active_orders"] = $history;
        echo json_encode($response);
    }

    public function unActiveOrders(Request $request) {
        $firebaseID = $request->input('firebase_id');
        $senderID = \App\Shoppers::where('firebase_id', $firebaseID)->select('id')->first();
        $history = \App\OrderDetails::where('customer_id', $senderID->id)
                ->where('order_details.order_status', 3)
                ->leftJoin('order_status', 'order_details.order_status', '=', 'order_status.id')
                ->groupBY('orderNumber')
                ->selectRaw('order_status.order_status,(Count(product_id)) as no_of_products, sum(total) as totalsum, delivery_cost, orderNumber')
                ->get();
        $response["result"] = "success";
        $response["unactive_orders"] = $history;
        echo json_encode($response);
    }

    public function slider() {
        $slider = \App\Slider::select('slider_name', 'image_url', 'brand_id')->get();
        $response["result"] = "success";
        $response["slider"] = $slider;
        echo json_encode($response);
    }

    public function filter(Request $request) {
        $maxprice = $request->input('maxprice');
        $minprice = $request->input('minprice');

        $products = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                        ->join('brands', 'products.brand_id', '=', 'brands.id')
                        ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                        ->where('products.activated', 1)
                        ->whereBetween('products.price', [$minprice, $maxprice])->get();
        $response["result"] = "success";
        $response["products"] = $products;
        echo json_encode($response);
    }

    public function wishlistpost(Request $request) {
        $productId = $request->input('product_id');
        $firebaseId = $request->input('firebase_id');
        $senderID = \App\Shoppers::where('firebase_id', $firebaseId)->select('id')->first();
        $wishlistcheck = \App\Wishlist::where('product_id', $productId)->where('shoper_id', $senderID->id)->first();
        if ($wishlistcheck->id) {
            $wishlist = new \App\Wishlist();
            $wishlist->product_id = $productId;
            $wishlist->shoper_id = $senderID->id;
            $wishlist->save();
            $response["result"] = "success";
            $response["message"] = "Product has been added to wish list successfully";
            echo json_encode($response);
        } else {
            $response["result"] = "success";
            $response["message"] = "Product is already in your wishlist";
            echo json_encode($response);
        }
    }

    public function wishlistDisplay(Request $request) {
        $firebaseId = $request->input('firebase_id');
        $senderID = \App\Shoppers::where('firebase_id', $firebaseId)->select('id')->first();
        $wishlist = \App\Wishlist::join('products', 'wishlist.product_id', '=', 'products.id')
                ->join('category', 'products.category_id', '=', 'category.id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->select('products.fragrance_ml', 'products.day_display', 'products.date_display', 'products.watch_type', 'products.face_color', 'products.straps', 'products.likes', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available')
                ->where('products.activated', 1)
                ->where('wishlist.shoper_id', $senderID->id)
                ->get();
        $response["result"] = "success";
        $response["wishlist"] = $wishlist;
        echo json_encode($response);
    }

    public function wishlistRemove(Request $request) {
        $firebaseId = $request->input('firebase_id');
        $productId = $request->input('product_id');
        $senderID = \App\Shoppers::where('firebase_id', $firebaseId)->select('id')->first();
        $wishlist = \App\Wishlist::where('product_id', $productId)->where('shoper_id', $senderID->id)->first();
        $wishlist->delete();
        $response["result"] = "success";
        $response["message"] = "Item was removed from wishlist successfully";
        echo json_encode($response);
    }

    public function checkifwishlisted(Request $request) {
        $firebaseId = $request->input('firebase_id');
        $productId = $request->input('product_id');
        $senderID = \App\Shoppers::where('firebase_id', $firebaseId)->select('id')->first();
        $wishlist = \App\Wishlist::where('product_id', $productId)->where('shoper_id', $senderID->id)->first();
        if ($wishlist->id) {
            $response["result"] = "success";
            $response["message"] = "Product is already wishlisted";
            echo json_encode($response);
        } else {
            $response["result"] = "failure";
            $response["message"] = "Product is not already wishlisted";
            echo json_encode($response);
        }
    }

    public function getBrandsByCategory(Request $request) {
        $categoryId = $request->input('category_id');
        $brandsCat = \App\Brand::where('category_id', $categoryId)
                        ->select('image', 'brand_name', 'id as brandId')->get();
        $response["result"] = "success";
        $response["brands"] = $brandsCat;
        echo json_encode($response);
    }

    public function getTagsByCategory(Request $request) {
        $categoryId = $request->input('category_id');
        $tagsCat = \App\Tags::where('category_id', $categoryId)
                        ->select('id as tagID', 'tag_name')->inRandomOrder()->get();
        $response["result"] = "success";
        $response["tags"] = $tagsCat;
        echo json_encode($response);
    }

}
