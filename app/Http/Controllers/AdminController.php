<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mail;
use Validator;
use App\Mail\EmailVerification;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Yajra\Datatables\Datatables;

class AdminController extends Controller {

    //settings
    public function settings() {
        $title = "Settings";
        $settings = \App\Settings::first();
        return view('backend.manage_settings', compact('settings', 'title'));
    }

    public function settingsAdd(Request $request) {
        $inputs = $request->all();

        $rule = array(
            'site_name' => 'required',
            'description' => 'required',
            'site_email' => 'required|email',
            'site_phone' => 'required',
            'location' => 'required',
        );

        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $settings = \App\Settings::findOrFail($request->input('inputId'));
        } else {
            $settings = new \App\Settings;
        }

        $settings->site_name = $request->input('site_name');

        $settings->description = $request->input('description');

        $settings->site_email = $request->input('site_email');

        $settings->site_phone = $request->input('site_phone');

        $settings->address = $request->input('location');

        $settings->save();

        if (!empty($inputs['id'])) {
            \Session::flash('flash_message', 'Changes Saved');

            return back();
        } else {
            \Session::flash('flash_message', 'Settings added successfully');

            return back();
        }
    }

    //Products
    public function products() {
        $title = "Producs";
        $category = \App\Category::get();
        $brand = \App\Brand::get();
        $tags = \App\Tags::get();
        $size = \App\Size::get();
        $colors = \App\Colors::get();
        return view('backend.manage_products', compact('title', 'category', 'brand', 'tags', 'size', 'colors'));
    }

    public function productsAdd(Request $request) {
        $inputs = $request->all();

        $rule = array(
            'inputProductName' => 'required',
            'selectCategory' => 'required',
            'selectBrand' => 'required',
            'inputPrice' => 'required',
        );

        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\Product::findOrFail($request->input('inputId'));
            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/product/' . $row->image);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/product';

                $request->file('imageFile')->move($path, $filename);

                $row->image = url('/') . '/' . $path . '/' . $filename;
            }
            $tagspro = \App\TagsProducts::where('product_id', $row->id)
                    ->get();
            foreach ($tagspro as $tagsdelete) {
                $deleteTags = \App\TagsProducts::findorFail($tagsdelete->id);
                $deleteTags->delete();
            }
            $row->slug = $this->createSlug($request->input('inputProductName'), $row->id);
        } else {
            $row = new \App\Product;
            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/product/' . $row->image);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/product';

                $request->file('imageFile')->move($path, $filename);

                $row->image = url('/') . '/' . $path . '/' . $filename;
            }
            $row->slug = $this->createSlug($request->input('inputProductName'));
        }
        $size_entries = array();
        $color_entries = array();
        $ml_entries = array();
         $face_entries = array();
        $size_item = $request->input('size');
        $color_item = $request->input('colors');
        $ml_item = $request->input('inputml');
        $face_item = $request->input('facecolors');
        $number_of_size_entries = sizeof($size_item);
        $number_of_color_entries = sizeof($color_item);
        $number_of_ml_entries = sizeof($ml_item);
        $number_of_face_entries = sizeof($face_item);
        for ($i = 0; $i < $number_of_size_entries; $i++) {
            if ($size_item[$i] != "") {
                $new_entry = array($size_item[$i]);
                array_push($size_entries, $new_entry);
            }
        }

        for ($i = 0; $i < $number_of_color_entries; $i++) {
            if ($color_item[$i] != "") {
                $new_entry_color = array($color_item[$i]);
                array_push($color_entries, $new_entry_color);
            }
        }
        
         for ($i = 0; $i < $number_of_ml_entries; $i++) {
            if ($ml_item[$i] != "") {
                $new_entry_ml = array($ml_item[$i]);
                array_push($ml_entries, $new_entry_ml);
            }
        }
        
         for ($i = 0; $i < $number_of_face_entries; $i++) {
            if ($ml_item[$i] != "") {
                $new_entry_face = array($face_item[$i]);
                array_push($face_entries, $new_entry_face);
            }
        }

        $row->size = json_encode($size_entries);
        $row->color = json_encode($color_entries);
        $row->fragrance_ml = json_encode($ml_entries);
        $row->face_color = json_encode($face_entries);
        $row->productName = $request->input('inputProductName');
        $row->productDescription = $request->input('inputProductDescription');
        $row->category_id = $request->input('selectCategory');
        $row->brand_id = $request->input('selectBrand');
        $row->price = $request->input('inputPrice');
        $row->old_price = $request->input('inputOldPrice');
        
        
        $row->units_available = $request->input('inputUnitsAvailable');
        
        $row->straps = $request->input('inputstraps');
        $row->watch_type = $request->input('watchtype');
        $row->date_display = $request->input('datedisplay');
        $row->day_display = $request->input('daydisplay');

        $row->save();

        $proTags = $request->input('tags');
        $number_of_tags_entries = sizeof($proTags);
        if (!empty($request->input('inputId'))) {
            
        }
        $deleteTags = \App\TagsProducts::where('product_id',$row->id)->get();
        foreach($deleteTags as $tgdelete){
            $del = \App\TagsProducts::findorFail($tgdelete->id);
            $del->delete();
        }
        for ($j = 0; $j < $number_of_tags_entries; $j++) {
            if ($proTags[$j] != "") {

                $tagsproducts = new \App\TagsProducts();
                $tagsproducts->product_id = $row->id;
                $tagsproducts->tag_id = $proTags[$j];
                $tagsproducts->save();
            }
        }


        \Session::flash('flash_message', 'Changes Saved successfully');

        return back();
    }

    public function editProducts($id) {

        $products = \App\Product::where('id', $id)->first();
        $category = \App\Category::get();
        $brand = \App\Brand::get();
        $tags = \App\Tags::get();
        $size = \App\Size::get();
        $colors = \App\Colors::get();
        $ml = \App\ML::get();
        $title = "Edit " . $products->productName;

        return view('backend.edit_product', compact('ml','title', 'category', 'brand', 'tags', 'size', 'colors', 'products'));
    }

    public function addProducts() {
        $category = \App\Category::get();
        $brand = \App\Brand::get();
        $tags = \App\Tags::get();
        $size = \App\Size::get();
        $colors = \App\Colors::get();
        $ml = \App\ML::get();
        $title = "Add a new product";

        return view('backend.add_product', compact('ml','title', 'category', 'brand', 'tags', 'size', 'colors'));
    }

    public function productsTable() {
        $row = \App\Product::join('category', 'products.category_id', '=', 'category.id')
                ->orderBY('products.id','DESC')
                ->select('products.slug', 'products.featured', 'products.activated', 'products.id', 'category.category_name', 'products.image', 'products.productName', 'products.price', 'products.old_price');

        return Datatables::of($row)
                        ->addColumn('action', function ($row) {
                            $images = '<li><a href="' . route('productImages', ['id' => $row->id]) . '" role="menuitem"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Add images</a></li>';

                            if ($row->featured == 0) {
                                $feature = '<li><a href="' . route('product.feature', ['id' => $row->id]) . '" role="menuitem"><i class="glyphicon glyphicon-cog" aria-hidden="true"></i> Feature</a></li>';
                            } else {
                                $feature = '<li><a href="' . route('product.unfeature', ['id' => $row->id]) . '" role="menuitem"><i class="glyphicon glyphicon-asterisk" aria-hidden="true"></i> Un Feature</a></li>';
                            }

                            if ($row->activated == 0) {
                                $active = '<li><a href="' . route('product.activate', ['id' => $row->id]) . '" role="menuitem"><i class="glyphicon glyphicon-ok" aria-hidden="true"></i> Activate</a></li>';
                            } else {
                                $active = '<li><a href="' . route('product.deactivate', ['id' => $row->id]) . '" role="menuitem"><i class="glyphicon glyphicon-remove" aria-hidden="true"></i> Deactivate</a></li>';
                            }

                            $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href=" ' . route('product.edit', ['id' => $row->id]) . ' " role="menuitem"><i class="glyphicon glyphicon-edit" aria-hidden="true"></i> Edit</a></li>
                                                        ' . $images . '
                                                        ' . $feature . '
                                                        ' . $active . '
                                                        <li><a onclick="return checkDelete();" href="' . route('productsdelete', ['id' => $row->id]) . '"><i class="icon-trash"></i> Delete</a></li>
                                                        </ul>
                                                </li>
                                            </ul>';

                            return $endDropdown;
                        })
                        ->make(true);
    }

    public function productDetails($id) {
        $row = \App\Product::findorFail($id);
        return \Response::json($row);
    }

    public function productTags($id) {
        $tags = \App\TagsProducts::where('product_id', $id)
                ->select('tag_id')
                ->get();

        return \Response::json($tags);
    }

    public function productDelete($id) {
        $row = \App\Product::findorFail($id);
        $row->delete();
        $delTags = \App\TagsProducts::where('product_id',$id)->get();
        foreach($delTags as $deleteTags){
            $del = \App\TagsProducts::findorFail($deleteTags->id);
            $del->delete();
        }
        \Session::flash('flash_message', 'Product was deleted successfully');
        return back();
    }

    public function productFeature($id) {
        $row = \App\Product::findorFail($id);
        $row->featured = 1;
        $row->save();
        \Session::flash('flash_message', 'Product has been featured successfully');
        return back();
    }

    public function productUnfeature($id) {
        $row = \App\Product::findorFail($id);
        $row->featured = 0;
        $row->save();
        \Session::flash('flash_message', 'Product has been un-featured successfully');
        return back();
    }

    public function productActivate($id) {
        $row = \App\Product::findorFail($id);
        $row->activated = 1;
        $row->save();
        \Session::flash('flash_message', 'Product has been activated successfully');
        return back();
    }

    public function productDeactivate($id) {
        $row = \App\Product::findorFail($id);
        $row->activated = 0;
        $row->save();
        \Session::flash('flash_message', 'Product has been de activated successfully');
        return back();
    }

    public function createSlug($title, $id = 0) {
        // Normalize the title
        $slug = str_slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugsActivity($slug, $id);
        // If we haven't used it before then we are all good.
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }
        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugsActivity($slug, $id = 0) {
        return \App\Product::select('slug')->where('slug', 'like', $slug . '%')
                        ->where('id', '<>', $id)
                        ->get();
    }

    //Category
    public function category() {
        $title = "Category";
        return view('backend.manage_category', compact('title'));
    }

    public function categoryAdd(Request $request) {
        $inputs = $request->all();

        $rule = array(
            'inputCategoryName' => 'required',
            'inputCategoryDescription' => 'required',
        );

        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\Category::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/category/' . $row->image);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/category';

                $request->file('imageFile')->move($path, $filename);

                $row->image = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \App\Category;

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/category/' . $row->image);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/category';

                $request->file('imageFile')->move($path, $filename);

                $row->image = url('/') . '/' . $path . '/' . $filename;
            }
        }

        $row->category_name = $request->input('inputCategoryName');
        $row->description = $request->input('inputCategoryDescription');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function categoryTable() {
        $cat = \App\Category::select('id', 'category_name', 'image')->orderBY('id','DESC');
        return Datatables::of($cat)
                        ->addColumn('action', function ($cat) {
                            $rend = '<a href="javascript: ajaxmodaledit(' . $cat->id . ')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                            return $rend;
                        })
                        ->make(true);
    }

    public function categoryDetails($id) {
        $row = \App\Category::findorFail($id);
        return \Response::json($row);
    }

    public function categoryDelete($id) {
        $row = \App\Category::findorFail($id);
        $row->delete();
        \Session::flash('flash_message', 'Category deleted successfully');
        return back();
    }

    public function brand() {
        $title = "Brands";
        $category = \App\Category::get();
        return view('backend.manage_brand', compact('title','category'));
    }

    public function brandAdd(Request $request) {
        $inputs = $request->all();

        $rule = array(
            'inputBrandName' => 'required',
            'inputBrandDescription' => 'required',
        );

        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\Brand::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/brands/' . $row->image);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/brands';

                $request->file('imageFile')->move($path, $filename);

                $row->image = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \App\Brand;

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/brands/' . $row->image);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/brands';

                $request->file('imageFile')->move($path, $filename);

                $row->image = url('/') . '/' . $path . '/' . $filename;
            }
        }

        $row->brand_name = $request->input('inputBrandName');
        $row->category_id = $request->input('selectCategory');
        $row->description = $request->input('inputBrandDescription');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function brandTable() {
        $row = \App\Brand::leftJoin('category','brands.category_id','=','category.id')
                ->select('brands.id', 'brands.image', 'brands.brand_name','category.category_name')->orderBY('brands.id','DESC');;
        return Datatables::of($row)
                        ->addColumn('action', function ($row) {
                            $rend = '<a href="javascript: ajaxmodaledit(' . $row->id . ')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                            $rend2 = ' <a href="' . route('brand.delete', ['id' => $row->id]) . '" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                            $final = $rend . $rend2;
                            return $final;
                        })
                        ->make(true);
    }
    
    public function getBrandsByCategory(Request $request){
        $input = $request->input('option');
        $consts = \App\Brand::where('category_id',$input)->get();
        return \Response::json($consts);
    }

    public function brandDetails($id) {
        $row = \App\Brand::findorFail($id);
        return \Response::json($row);
    }

    public function brandDelete($id) {
        $row = \App\Brand::findorFail($id);
        $row->delete();
        \Session::flash('flash_message', 'Brand deleted successfully');
        return back();
    }

    //Adverts
    public function adverts() {
        $title = "Adverts";
        $brands = \App\Brand::get();
        return view('backend.manage_adverts', compact('title', 'brands'));
    }

    public function advertsAdd(Request $request) {
        $inputs = $request->all();

        $rule = array(
            'selectName' => 'required',
        );

        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\Adverts::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/adverticement/' . $row->image);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/adverticement';

                $request->file('imageFile')->move($path, $filename);

                $row->image = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \App\Adverts;

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/adverticement/' . $row->image);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/adverticement';

                $request->file('imageFile')->move($path, $filename);

                $row->image = url('/') . '/' . $path . '/' . $filename;
            }
        }

        $row->brand_id = $request->input('selectName');

        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function advertsDelete($id) {
        $row = \App\Adverts::findorFail($id);
        $row->delete();
        \Session::flash('flash_message', 'Advert deleted successfully');
        return back();
    }

    public function advertsTable() {
        $row = \App\Adverts::join('brands', 'adss.brand_id', '=', 'brands.id')
                ->select('adss.id', 'adss.image', 'brands.brand_name');
        return Datatables::of($row)
                        ->addColumn('action', function ($row) {
                            $rend = '<a onclick="return checkDelete();" href="' . route('adverts.delete', ['id' => $row->id]) . '" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                            return $rend;
                        })
                        ->make(true);
    }

    /**
     * Product Images
     */
    public function productImages($id) {
        $title = "Product Images";
        $products = \App\Product::findorFail($id);
        return View('backend.manage_product_images', compact('title', 'products'));
    }

    public function productImagesTable($id) {
        $productsImages = \App\ProductsImages::select('id', 'image_url')->where('product_id', $id);
        return Datatables::of($productsImages)
                        ->addColumn('action', function ($productsImages) {
                            $rend = '<a onclick="return checkDelete();" href="' . route('productImagesDelete', ['id' => $productsImages->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                            return $rend;
                        })
                        ->make(true);
    }

    public function productImagesDelete($id) {
        $productsimages = \App\ProductsImages::findorFail($id);
        \File::delete(public_path() . '/packages/uploads/productsimages/' . $productsimages->image_url);
        $productsimages->delete($id);
        \Session::flash('flash_message', 'Products images deleted successfully');
        return back();
    }

    public function productImagesAdd(Request $request) {
        $inputs = $request->all();
        $rule = array(
            'imageFile' => 'required',
        );
        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\ProductsImages::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/productsimages/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/productsimages';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \App\ProductsImages;

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/productsimages/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/productsimages';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        }
        $row->product_id = $request->input('product_id');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function tags() {
        $title = "Tags";
        $category = \App\Category::get();
        return view('backend.manage_tags', compact('title','category'));
    }

    public function tagsTable() {
        $tags = \App\Tags::leftJoin('category','tags.category_id','=','category.id')
                ->select('tags.id', 'tags.tag_name','category.category_name')->orderBY('id','DESC');
        return Datatables::of($tags)
                        ->addColumn('action', function ($tags) {
                            $delete = ' <a onclick="return checkDelete();" href="' . route('tags.delete', ['id' => $tags->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                            $edit = '<a  href="javascript: ajaxmodaledit(' . $tags->id . ')" class="btn btn-xs btn-primary"  ><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
                            return $edit;
                        })
                        ->make(true);
    }

    public function tagsAdd(Request $request) {
        $inputs = $request->all();
        $rule = array(
            'tag_name' => 'required',
        );
        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\Tags::findOrFail($request->input('inputId'));
        } else {
            $row = new \App\Tags();
        }
        $row->tag_name = $request->input('tag_name');
        $row->category_id = $request->input('selectCategory');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function tagsDetails($id) {
        $tags = \App\Tags::findorFail($id);
        return \Response::json($tags);
    }

    public function tagsDelete($id) {
        $tags = \App\Tags::findorFail($id);
        $tags->delete();
        \Session::flash('flash_message', 'Tag deleted successfully');
        return back();
    }

    /**
     * Colors
     */
    public function colors() {
        $title = "Colors";
        return view('backend.manage_colors', compact('title'));
    }

    public function colorsTable() {
        $colors = \App\Colors::select('id', 'color_name');
        return Datatables::of($colors)
                        ->addColumn('action', function ($colors) {
                            $delete = ' <a onclick="return checkDelete();" href="' . route('colors.delete', ['id' => $colors->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                            $edit = '<a  href="javascript: ajaxmodaledit(' . $colors->id . ')" class="btn btn-xs btn-primary"  ><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
                            return $edit . $delete;
                        })
                        ->make(true);
    }

    public function colorsAdd(Request $request) {
        $inputs = $request->all();
        $rule = array(
            'color_name' => 'required',
        );
        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\Colors::findOrFail($request->input('inputId'));
        } else {
            $row = new \App\Colors();
        }
        $row->color_name = $request->input('color_name');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function colorsDetails($id) {
        $color = \App\Colors::findorFail($id);
        return \Response::json($color);
    }

    public function colorsDelete($id) {
        $color = \App\Colors::findorFail($id);
        $color->delete();
        \Session::flash('flash_message', 'Color deleted successfully');
        return back();
    }

    /**
     * Size
     */
    public function size() {
        $title = "Size";
        return view('backend.manage_size', compact('title'));
    }

    public function sizeTable() {
        $size = \App\Size::select('id', 'size');
        return Datatables::of($size)
                        ->addColumn('action', function ($size) {
                            $delete = ' <a onclick="return checkDelete();" href="' . route('size.delete', ['id' => $size->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                            $edit = '<a  href="javascript: ajaxmodaledit(' . $size->id . ')" class="btn btn-xs btn-primary"  ><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
                            return $edit . $delete;
                        })
                        ->make(true);
    }

    public function sizeAdd(Request $request) {
        $inputs = $request->all();
        $rule = array(
            'size' => 'required',
        );
        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\Size::findOrFail($request->input('inputId'));
        } else {
            $row = new \App\Size();
        }
        $row->size = $request->input('size');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function sizeDetails($id) {
        $size = \App\Size::findorFail($id);
        return \Response::json($size);
    }

    public function sizeDelete($id) {
        $size = \App\Size::findorFail($id);
        $size->delete();
        \Session::flash('flash_message', 'Size deleted successfully');
        return back();
    }

    /**
     * Payment Method
     */
    public function paymentMethod() {
        $title = "Payment";
        return view('backend.manage_payment', compact('title'));
    }

    public function paymentMethodTable() {
        $payment = \App\PaymentMethod::select('id', 'payment_method');
        return Datatables::of($payment)
                        ->addColumn('action', function ($payment) {
                            $delete = ' <a onclick="return checkDelete();" href="' . route('payment.delete', ['id' => $payment->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                            $edit = '<a  href="javascript: ajaxmodaledit(' . $payment->id . ')" class="btn btn-xs btn-primary"  ><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
                            return $edit . $delete;
                        })
                        ->make(true);
    }

    public function paymentMethodAdd(Request $request) {
        $inputs = $request->all();
        $rule = array(
            'payment_method' => 'required',
        );
        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\PaymentMethod::findOrFail($request->input('inputId'));
        } else {
            $row = new \App\PaymentMethod();
        }
        $row->payment_method = $request->input('payment_method');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function paymentMethodDetails($id) {
        $size = \App\PaymentMethod::findorFail($id);
        return \Response::json($size);
    }

    public function paymentMethodDelete($id) {
        $size = \App\PaymentMethod::findorFail($id);
        $size->delete();
        \Session::flash('flash_message', 'Size deleted successfully');
        return back();
    }

    /**
     * Orders
     */
    public function orders() {
        $title = "Orders";
        return view('backend.manage_orders', compact('title'));
    }

    public function ordersTable() {
        $history = \App\OrderDetails::groupBY('orderNumber')
                ->leftJoin('order_status', 'order_details.order_status', '=', 'order_status.id')
                ->leftJoin('shoppers', 'order_details.customer_id', '=', 'shoppers.id')
                ->selectRaw('shoppers.contact,shoppers.name,(Count(product_id)) as no_of_products, sum(total) as totalsum, delivery_cost, orderNumber,shipping_address,order_status.order_status,order_details.paid');
        return Datatables::of($history)
                        ->addColumn('action', function ($history) {
                            if ($history->paid == 0) {
                                $pay = '<li><a href=" ' . route('order.payment.change', ['id' => $history->orderNumber, 'payment' => 1]) . ' " role="menuitem"><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Paid</a></li>';
                            } else {
                                $pay = '<li><a href=" ' . route('order.payment.change', ['id' => $history->orderNumber, 'payment' => 0]) . ' " role="menuitem"><i class="glyphicon glyphicon-remove-circle" aria-hidden="true"></i> Not Paid</a></li>';
                            }

                            $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href=" ' . route('order.details', ['id' => $history->orderNumber]) . ' " role="menuitem"><i class="glyphicon glyphicon-star" aria-hidden="true"></i> Order details</a></li>
                                                            
                                                        ' . $pay . '
                                                        
                                                        </ul>
                                                </li>
                                            </ul>';


                            return $endDropdown;
                        })
                        ->make(true);
    }

    public function ordersOrdered() {
        $title = "Orders Ordered";
        return view('backend.manage_orders_ordered', compact('title'));
    }

    public function ordersOrderedTable() {
        $history = \App\OrderDetails::groupBY('orderNumber')
                ->where('order_details.order_status', 1)
                ->leftJoin('shoppers', 'order_details.customer_id', '=', 'shoppers.id')
                ->leftJoin('order_status', 'order_details.order_status', '=', 'order_status.id')
                ->selectRaw('shoppers.contact,shoppers.name,(Count(product_id)) as no_of_products, sum(total) as totalsum, delivery_cost, orderNumber,shipping_address,order_status.order_status,order_details.paid');
        return Datatables::of($history)
                        ->addColumn('action', function ($history) {
                            if ($history->paid == 0) {
                                $pay = '<li><a href=" ' . route('order.payment.change', ['id' => $history->orderNumber, 'payment' => 1]) . ' " role="menuitem"><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Paid</a></li>';
                            } else {
                                $pay = '<li><a href=" ' . route('order.payment.change', ['id' => $history->orderNumber, 'payment' => 0]) . ' " role="menuitem"><i class="glyphicon glyphicon-remove-circle" aria-hidden="true"></i> Not Paid</a></li>';
                            }
                            $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href=" ' . route('order.details', ['id' => $history->orderNumber]) . ' " role="menuitem"><i class="glyphicon glyphicon-star" aria-hidden="true"></i> Order details</a></li>
                                                        <li><a href=" ' . route('order.status.change', ['id' => $history->orderNumber, 'status' => 2]) . ' " role="menuitem"><i class="icon-car" aria-hidden="true"></i> Shipped</a></li>
                                                         ' . $pay . '
                                                        </ul>
                                                </li>
                                            </ul>';

                            return $endDropdown;
                        })
                        ->make(true);
    }

    public function ordersShipped() {
        $title = "Orders Shipped";
        return view('backend.manage_orders_shipped', compact('title'));
    }

    public function ordersShipedTable() {
        $history = \App\OrderDetails::groupBY('orderNumber')
                ->where('order_details.order_status', 2)
                ->leftJoin('shoppers', 'order_details.customer_id', '=', 'shoppers.id')
                ->leftJoin('order_status', 'order_details.order_status', '=', 'order_status.id')
                ->selectRaw('shoppers.contact,shoppers.name,(Count(product_id)) as no_of_products, sum(total) as totalsum, delivery_cost, orderNumber,shipping_address,order_status.order_status,order_details.paid');
        return Datatables::of($history)
                        ->addColumn('action', function ($history) {
                            if ($history->paid == 0) {
                                $pay = '<li><a href=" ' . route('order.payment.change', ['id' => $history->orderNumber, 'payment' => 1]) . ' " role="menuitem"><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Paid</a></li>';
                            } else {
                                $pay = '<li><a href=" ' . route('order.payment.change', ['id' => $history->orderNumber, 'payment' => 0]) . ' " role="menuitem"><i class="glyphicon glyphicon-remove-circle" aria-hidden="true"></i> Not Paid</a></li>';
                            }
                            $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href=" ' . route('order.details', ['id' => $history->orderNumber]) . ' " role="menuitem"><i class="glyphicon glyphicon-star" aria-hidden="true"></i> Order details</a></li>
                                                        <li><a href=" ' . route('order.status.change', ['id' => $history->orderNumber, 'status' => 3]) . ' " role="menuitem"><i class="glyphicon glyphicon-ok" aria-hidden="true"></i> Received</a></li>
                                                         ' . $pay . '
                                                        </ul>
                                                </li>
                                            </ul>';

                            return $endDropdown;
                        })
                        ->make(true);
    }

    public function ordersReceived() {
        $title = "Orders Received";
        return view('backend.manage_orders_received', compact('title'));
    }

    public function ordersReceivedTable() {
        $history = \App\OrderDetails::groupBY('orderNumber')
                ->where('order_details.order_status', 3)
                ->leftJoin('shoppers', 'order_details.customer_id', '=', 'shoppers.id')
                ->leftJoin('order_status', 'order_details.order_status', '=', 'order_status.id')
                ->selectRaw('shoppers.contact,shoppers.name,(Count(product_id)) as no_of_products, sum(total) as totalsum, delivery_cost, orderNumber,shipping_address,order_status.order_status,order_details.paid');
        return Datatables::of($history)
                        ->addColumn('action', function ($history) {
                            if ($history->paid == 0) {
                                $pay = '<li><a href=" ' . route('order.payment.change', ['id' => $history->orderNumber, 'payment' => 1]) . ' " role="menuitem"><i class="glyphicon glyphicon-ok-circle" aria-hidden="true"></i> Paid</a></li>';
                            } else {
                                $pay = '<li><a href=" ' . route('order.payment.change', ['id' => $history->orderNumber, 'payment' => 0]) . ' " role="menuitem"><i class="glyphicon glyphicon-remove-circle" aria-hidden="true"></i> Not Paid</a></li>';
                            }
                            $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href=" ' . route('order.details', ['id' => $history->orderNumber]) . ' " role="menuitem"><i class="glyphicon glyphicon-star" aria-hidden="true"></i> Order details</a></li>
                                                        
                                                         ' . $pay . '
                                                        </ul>
                                                </li>
                                            </ul>';

                            return $endDropdown;
                        })
                        ->make(true);
    }

    public function changeOrderStatus($id, $status) {
        $order = \App\OrderDetails::where('orderNumber', $id)->get();
        foreach ($order as $row) {
            $row2 = \App\OrderDetails::findorFail($row->id);
            $row2->order_status = $status;
            $row2->save();
        }
        \Session::flash('flash_message', 'Order status changed successfully');
        return back();
    }

    public function changePaymentStatus($id, $payment) {
        $order = \App\OrderDetails::where('orderNumber', $id)->get();
        foreach ($order as $row) {
            $row2 = \App\OrderDetails::findorFail($row->id);
            $row2->paid = $payment;
            $row2->save();
        }
        \Session::flash('flash_message', 'Payment status changed successfully');
        return back();
    }

    public function orderDetailsView($orderNum) {
        $title = 'Order Details';
        return view('backend.manage_order_details', compact('title', 'orderNum'));
    }

    public function orderDetailsTable($id) {
        $order = \App\OrderDetails::where('orderNumber', $id)
                ->leftJoin('products', 'order_details.product_id', '=', 'products.id')
                ->leftJoin('category', 'products.category_id', '=', 'category.id')
                ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
                ->select('order_details.color as cl', 'order_details.size as detailSize', 'order_details.total', 'order_details.quantity as qty', 'products.brand_id as brandId', 'products.category_id as categoryId', 'products.id as productId', 'category.category_name', 'products.productName', 'products.productDescription', 'brands.brand_name', 'products.image', 'products.price', 'products.old_price', 'products.size', 'products.color', 'products.units_available');




        return Datatables::of($order)
                        ->addColumn('action', function ($order) {

                            $endDropdown = '<ul class="icons-list">
                                                <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                        </a>

                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href=" ' . route('order.details', ['id' => $order->orderNumber]) . ' " role="menuitem"><i class="glyphicon glyphicon-star" aria-hidden="true"></i> Order details</a></li>
                                                      
                                                        </ul>
                                                </li>
                                            </ul>';

                            return $endDropdown;
                        })
                        ->make(true);
    }

    /**
     * Order delivery status
     */
    public function orderStatus() {
        $title = "Delivery Status";
        return view('backend.manage_order_status', compact('title'));
    }

    public function orderStatusTable() {
        $order = \App\OrderStatus::select('id', 'order_status');
        return Datatables::of($order)
                        ->addColumn('action', function ($order) {
                            $delete = ' <a onclick="return checkDelete();" href="' . route('order.status.delete', ['id' => $order->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                            $edit = '<a  href="javascript: ajaxmodaledit(' . $order->id . ')" class="btn btn-xs btn-primary"  ><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
                            return $edit . $delete;
                        })
                        ->make(true);
    }

    public function orderStatusAdd(Request $request) {
        $inputs = $request->all();
        $rule = array(
            'order_status' => 'required',
        );
        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\OrderStatus::findOrFail($request->input('inputId'));
        } else {
            $row = new \App\OrderStatus();
        }
        $row->order_status = $request->input('order_status');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function orderStatusDetails($id) {
        $order = \App\OrderStatus::findorFail($id);
        return \Response::json($order);
    }

    public function orderStatusDelete($id) {
        $order = \App\OrderStatus::findorFail($id);
        $order->delete();
        \Session::flash('flash_message', 'order deleted successfully');
        return back();
    }

    /**
     * Slider
     */
    public function slider() {
        $title = "Slider";
        $brands = \App\Brand::get();
        return view('backend.manage_slider', compact('title', 'brands'));
    }

    public function sliderTable() {
        $row = \App\Slider::join('brands', 'slider.brand_id', '=', 'brands.id')
                ->select('slider.id', 'image_url', 'brands.brand_name');
        return Datatables::of($row)
                        ->addColumn('action', function ($row) {
                            $edit = ' <a  href="javascript: ajaxmodaledit(' . $row->id . ')" class="btn btn-xs btn-primary"  ><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
                            $rend = '<a onclick="return checkDelete();" href="' . route('slider.delete', ['id' => $row->id]) . '" class="btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                            return $rend . $edit;
                        })
                        ->make(true);
    }

    public function sliderAddEdit(Request $request) {
        $inputs = $request->all();
        $rule = array(

            'brand' => 'required',
        );
        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\Slider::findOrFail($request->input('inputId'));

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/slider/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/slider';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        } else {
            $row = new \App\Slider;

            if ($request->hasFile('imageFile')) {
                \File::delete(public_path() . '/packages/uploads/slider/' . $row->image_url);
                $port = $request->file('imageFile');
                $extension = $port->getClientOriginalExtension();
                $filename = time() . "." . $extension;

                $path = 'packages/uploads/slider';

                $request->file('imageFile')->move($path, $filename);

                $row->image_url = url('/') . '/' . $path . '/' . $filename;
            }
        }
        $row->brand_id = $request->input('brand');
        $row->slider_name = $request->input('slider_name');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function sliderDetails($id) {
        $slider = \App\Slider::findorFail($id);
        return \Response::json($slider);
    }

    public function sliderDelete($id) {
        $slider = \App\Slider::findorFail($id);
        $slider->delete();
        \Session::flash('flash_message', 'slider deleted successfully');
        return back();
    }
    
    /**
     * Ml
     */
    
    public function ml(){
        $title = "Ml";
        return view('backend.manage_ml', compact('title'));
    }
    

    public function mlTable() {
        $ml = \App\ML::select('id', 'ml');
        return Datatables::of($ml)
                        ->addColumn('action', function ($ml) {
                            $delete = ' <a onclick="return checkDelete();" href="' . route('ml.del', ['id' => $ml->id]) . '" class="btn btn-xs btn-danger"  ><i class="glyphicon glyphicon-trash"></i> Delete</a>';
                            $edit = '<a  href="javascript: ajaxmodaledit(' . $ml->id . ')" class="btn btn-xs btn-primary"  ><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
                            return $edit . $delete;
                        })
                        ->make(true);
    }

    public function mlAdd(Request $request) {
        $inputs = $request->all();
        $rule = array(
            'ml' => 'required',
        );
        $validator = Validator::make($inputs, $rule);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator->messages());
        }

        if (!empty($request->input('inputId'))) {
            $row = \App\ML::findOrFail($request->input('inputId'));
        } else {
            $row = new \App\ML();
        }
        $row->ml = $request->input('ml');
        $row->save();
        \Session::flash('flash_message', 'Changes Saved successfully');
        return back();
    }

    public function mlDetails($id) {
        $ml = \App\ML::findorFail($id);
        return \Response::json($ml);
    }

    public function mlDelete($id) {
        $ml = \App\ML::findorFail($id);
        $ml->delete();
        \Session::flash('flash_message', 'Ml deleted successfully');
        return back();
    }
    
}
